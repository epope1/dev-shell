#!/bin/bash

git config --global user.email $GIT_EMAIL
git config --global user.name $GIT_USER

cp -R /tmp/.ssh /root/.ssh
chmod 700 /root/.ssh
chmod 644 /root/.ssh/id_rsa.pub
chmod 600 /root/.ssh/id_rsa

if [ ! -f /root/.ssh/known_hosts ]; then
    cp /tmp/known_hosts /root/.ssh/known_hosts
fi

/bin/bash
